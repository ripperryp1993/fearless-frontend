import React from "react";

class AttendConferenceForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            conferences: [],
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartsChange = this.handleStartsChange.bind(this);
        this.handleEndsChange = this.handleEndsChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this);
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    };

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            const cleared = {
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: '',
              };
              this.setState(cleared);
        };
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value});
    };

    handleStartsChange(event) {
        const value = event.target.value;
        this.setState({starts: value});
    };

    handleEndsChange(event) {
        const value = event.target.value;
        this.setState({ends: value});
    };

    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({description: value});
    };

    handleMaxPresentationsChange(event) {
        const value = event.target.value;
        this.setState({max_presentations: value});
    };

    handleMaxAttendeesChange(event) {
        const value = event.target.value;
        this.setState({max_attendees: value});
    };

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value});
    };

    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations});
        };
    };
    
    render() {
        return (
            <div class="my-5">
                <div class="row">
                    <div class="col col-sm-auto">
                        <img width="300" class="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg"/>
                    </div>
                    <div class="col">
                        <div class="card shadow">
                            <div class="card-body">
                            <form id="create-attendee-form">
                                <h1 class="card-title">It's Conference Time!</h1>
                                <p class="mb-3">
                                Please choose which conference
                                you'd like to attend.
                                </p>
                                <div class="d-flex justify-content-center mb-3" id="loading-conference-spinner">
                                <div class="spinner-grow text-secondary" role="status">
                                    <span class="visually-hidden">Loading...</span>
                                </div>
                                </div>
                                <div class="mb-3">
                                <select name="conference" id="conference" class="form-select d-none" required>
                                    <option value="">Choose a conference</option>
                                </select>
                                </div>
                                <p class="mb-3">
                                Now, tell us about yourself.
                                </p>
                                <div class="row">
                                <div class="col">
                                    <div class="form-floating mb-3">
                                    <input required placeholder="Your full name" type="text" id="name" name="name" class="form-control"/>
                                    <label for="name">Your full name</label>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-floating mb-3">
                                    <input required placeholder="Your email address" type="email" id="email" name="email" class="form-control"/>
                                    <label for="email">Your email address</label>
                                    </div>
                                </div>
                                </div>
                                <button class="btn btn-lg btn-primary">I'm going!</button>
                            </form>
                            <div class="alert alert-success d-none mb-0" id="success-message">
                                Congratulations! You're all signed up!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        )
    }
};

export default AttendConferenceForm;