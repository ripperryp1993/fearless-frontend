import React from 'react';
import Nav from './Nav';
import AttendeeList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import { link } from 'react-router-dom';
import MainPage from './MainPage';
import {
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom';
import PresentationForm from './PresentationForm';


function App(props) {
  /*if (props.attendees === undefined) {
    return null;
  };*/
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route index element={<MainPage />} />
          <Route path='attendees' element={<AttendeeList attendees={props.attendees} />} />
          <Route path='locations'>
            <Route path='new' element={<LocationForm />} />
          </Route>
          <Route path='conferences'>
            <Route path='new' element={<ConferenceForm />} />
          </Route>
          <Route path='presentations'>
            <Route path='new' element={<PresentationForm />} />
          </Route>
        </Routes>
        {/*<ConferenceForm />
        <LocationForm />
        /*<AttendeeList attendees={props.attendees} />*/}
    </BrowserRouter>
  );
}
export default App;
