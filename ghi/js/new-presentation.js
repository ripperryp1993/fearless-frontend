window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    const selectTag = document.getElementById('conference');
    if (response.ok) {
        const data = await response.json();
        //console.log(data);
        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        };
        //console.log(selectTag);
    };


    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        //console.log('need to submit the form data');
        const json = JSON.stringify(Object.fromEntries(new FormData(formTag)));
        //console.log(json);

        const presentationUrl = 'http://localhost:8000' + selectTag.options[selectTag.selectedIndex].value + 'presentations/';
        const fetchConfig = {
            method: 'POST',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newPresentation = await response.json();
            console.log(newPresentation);
        };
    });
});