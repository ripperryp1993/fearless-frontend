function createCard(name, description, pictureUrl, start, end, locationName) {
    return `
    <div class="card shadow p-3 mb-5 bg-body rounded" style="width: 18rem; margin: 1em;">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">${start} - ${end}</li>
        </ul>
    </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            const error_message = document.getElementById('error');
            error_message.classList.remove('d-none');
            const main = document.getElementById('main');
            main.classList.add('d-none')
            throw new Error('Response not ok');
        } else {
            const data = await response.json();
            let counter = 0;
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                const col_1 = document.querySelector('.col_1');
                const col_2 = document.querySelector('.col_2');
                const col_3 = document.querySelector('.col_3');
                if (detailResponse.ok) {
                    const detail = await detailResponse.json();
                    const title = detail.conference.name;
                    const description = detail.conference.description;
                    const pictureUrl = detail.conference.location.picture_url;
                    const start = new Date(detail.conference.starts).toISOString();
                    const end = new Date(detail.conference.ends).toISOString();
                    const locationName = detail.conference.location.name;
                    const html = createCard(title, description, pictureUrl, start, end, locationName);
                    if (counter % 3 === 0) {
                        col_1.innerHTML += html;

                    } else if (counter % 3 === 1) {
                        col_2.innerHTML += html;
                    } else {
                        col_3.innerHTML += html;
                    };
                    //console.log('detail:', detail);
                    //console.log('counter_modulo', counter % 3);
                    counter += 1;
                    const placeHolder = document.querySelector('.cardholder');
                    placeHolder.classList.add('d-none');
                };
            };
        };
    } catch(e) {

        console.error('error', e);
    }

});
